var express = require('express')
var router = express.Router()
var path = require('path')
var fs = require('fs')
var scrape = require(`${appRoot}/my_modules/scrape`)
var showdown = require('showdown')
var converter = new showdown.Converter()

router.get('/', function(req, res) {
  
  res.render('store/index', {
    htmlTitle: 'T-shirts | dfoster.org',
    description: 'T-shirts designed by David Alexander',
    items: scrape.dir('store')
  })
})

router.get('/:item', function(req, res) {
  const itemDir = path.join(`${appRoot}/content/store/`);
  const itemPath = path.join(itemDir + `${req.params.item}.md`);

  if (!fs.existsSync(itemPath)) {
    return res.redirect('/404');
  }

  const [ metaJson, body ] = fs.readFileSync(itemPath, 'utf8').split('META_SEPARATOR');
  const meta = JSON.parse(metaJson);
  
  res.render('store/item', {
    htmlTitle: 'Dallas Killed JFK T-shirt | dfoster.org',
    description: 'Dallas killed JFK and you should buy this trendy-ass t-shirt to show your fellow Dallasites you know whose really responsible for the assassination of John F. Kennedy. The Germans have to live every day knowing their countrymen were responsible for the death of millions of Jews, so why should Dallas be let off the hook for the death of JFK?',
    meta: meta,
    body: converter.makeHtml(body)
  })
})

router.get('/dallas-killed-jfk/sold-out', function(req, res) {

  res.render('store/index', {
    htmlTitle: 'Apparel by David Alexander | Sold out!',
    description: 'Apparel by David Alexander',
  })
})

module.exports = router