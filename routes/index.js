var express = require('express')
var router = express.Router()
var scrape = require(`${appRoot}/my_modules/scrape`)
var showdown = require('showdown')
var fs = require('fs')
var converter = new showdown.Converter()

router.get('/', function(req, res) {

  res.render('index', {
    htmlTitle: 'Poetry, short stories, and t-shirts | dfoster.org',
    description: 'The poetry, stories, and merch of David Alexander, a writer and software developer based in Dallas, Texas',
    poems: scrape.dir('poetry'),
    stories: scrape.dir('stories'),
    merch: scrape.dir('store')
  })
})

router.post('/apk', (req, res) => {
  res.json(req.body);
})

router.get('/about', function(req, res) {
  const body = fs.readFileSync(`${appRoot}/content/about.md`, 'utf8')
  res.render('static/about', {
    htmlTitle: 'About dfoster.org',
    description: 'dfoster.org is a website showcasing the art, poetry, and short stories of David Alexander',
    body: converter.makeHtml(body)
  })
})

router.get('/farmers-market', function(req, res) {
  res.render('static/marketfinder', {
    htmlTitle: `Find a Farmer's Market`,
    description: `Enter your zip code to find a farmer's market near you certified by the USDA!`
  })
})

router.get('/afterlife', function(req, res) {
  res.render('static/afterlife', {})
})

module.exports = router
