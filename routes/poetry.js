var express = require('express')
var router = express.Router()
var path = require('path')
var fs = require('fs')
var scrape = require(`${appRoot}/my_modules/scrape`)

router.get('/', function(req, res) {
  
  res.render('poetry/index', {
    htmlTitle: 'Poetry | dfoster.org',
    description: 'Poetry by David Alexander',
    poems: scrape.dir('poetry')
  })
})

router.get('/:poem', function(req, res) {
  const poetryDir = path.join(`${appRoot}/content/poetry/`);
  const poemPath = path.join(poetryDir + `${req.params.poem}.md`);

  if (!fs.existsSync(poemPath)) {
    return res.redirect('/404');
  }

  const [ metaJson, body ] = fs.readFileSync(poemPath, 'utf8').split('META_SEPARATOR');
  meta = JSON.parse(metaJson);
  
  res.render('poetry/poem', {
    htmlTitle: `${meta.title} | A poem about ${meta.about} | dfoster.org`,
    description: `${meta.title} is a poem by David Alexander about ${meta.about} `,
    body: body
  })
})

module.exports = router