var express = require('express')
var router = express.Router()
var path = require('path')
var fs = require('fs')

var photoDir = path.join(`${__dirname}/../public/media/photography/`)
var metaDir = path.join(`${__dirname}/../content/photography/`)

router.get('/', function(req, res) {

  var albumDirNames = fs.readdirSync(photoDir)

  res.render('photography/index', {
    htmlTitle: 'Photography',
    description: 'Photography by David Alexander',
    albums: albumDirNames
  })
})

router.get('/:album', function(req, res) {

  var albumDir = path.join(`${photoDir}/${req.params.album}`)
  var albumMetaDir = path.join(`${metaDir}/${req.params.album.replace(/-/g, ' ')}`)

  if (fs.existsSync(albumDir) && fs.existsSync(albumMetaDir)) {
    res.render('photography/album', {
      htmlTitle: req.params.album.replace(/-/g, ' '),
      description: 'A photo album by David Alexander',
      photos: fs.readdirSync(albumDir),
      meta: JSON.parse(fs.readFileSync(albumMetaDir))
    })
  }

  else
    res.redirect('/404')
})

module.exports = router