var express = require('express')
var router = express.Router()
var path = require('path')
var fs = require('fs')
var scrape = require(`${appRoot}/my_modules/scrape`)
var showdown = require('showdown')
var converter = new showdown.Converter()

router.get('/', function(req, res) {
  
  res.render('stories/index', {
    htmlTitle: 'Stories | dfoster.org',
    description: 'Short stories by David Alexander',
    stories: scrape.dir('stories')
  })
})

router.get('/:story', function(req, res) {
  const itemDir = path.join(`${appRoot}/content/stories/`);
  const itemPath = path.join(itemDir + `${req.params.story}.md`);

  if (!fs.existsSync(itemPath)) {
    return res.redirect('/404');
  }

  const [ metaJson, body ] = fs.readFileSync(itemPath, 'utf8').split('META_SEPARATOR');
  meta = JSON.parse(metaJson);
  
  res.render('stories/short', {
    htmlTitle: `${meta.title} | A short story about ${meta.about} | dfoster.org`,
    description: `${meta.title} is a short story by David Alexander about ${meta.about} `,
    body: converter.makeHtml(body)
  })
})

module.exports = router