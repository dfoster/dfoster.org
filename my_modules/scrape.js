const fs = require('fs');
const path = require('path');

exports.dir = function (dir) {
  const contentDir = `${appRoot}/content/${dir}/`;
  const contentFileNames = fs.readdirSync(contentDir);
  const content = [];

  contentFileNames.forEach(contentFileName => {
    const metaJson = fs.readFileSync(path.join(contentDir + contentFileName), 'utf8').split('META_SEPARATOR')[0];

    meta = JSON.parse(metaJson);
    meta.path = contentFileName.split('.')[0];

    content.push(meta);
  })
  return content;
}
