{
	"name": "Dallas Killed JFK",
	"paypal": "PGZvcm0gYWN0aW9uPSJodHRwczovL3d3dy5wYXlwYWwuY29tL2NnaS1iaW4vd2Vic2NyIiBtZXRob2Q9InBvc3QiIHRhcmdldD0iX3RvcCI+CjxpbnB1dCB0eXBlPSJoaWRkZW4iIG5hbWU9ImNtZCIgdmFsdWU9Il9zLXhjbGljayI+CjxpbnB1dCB0eXBlPSJoaWRkZW4iIG5hbWU9Imhvc3RlZF9idXR0b25faWQiIHZhbHVlPSJKNlNLUFc3NVBTTjhXIj4KPHRhYmxlPgo8dHI+PHRkPjxpbnB1dCB0eXBlPSJoaWRkZW4iIG5hbWU9Im9uMCIgdmFsdWU9IlNpemVzIj5TaXplczwvdGQ+PC90cj48dHI+PHRkPjxzZWxlY3QgbmFtZT0ib3MwIj4KCTxvcHRpb24gdmFsdWU9IlhTIj5YUyA8L29wdGlvbj4KCTxvcHRpb24gdmFsdWU9IlMiPlMgPC9vcHRpb24+Cgk8b3B0aW9uIHZhbHVlPSJNIj5NIDwvb3B0aW9uPgoJPG9wdGlvbiB2YWx1ZT0iTCI+TCA8L29wdGlvbj4KCTxvcHRpb24gdmFsdWU9IlhMIj5YTCA8L29wdGlvbj4KCTxvcHRpb24gdmFsdWU9IjJYTCI+MlhMIDwvb3B0aW9uPgo8L3NlbGVjdD4gPC90ZD48L3RyPgo8L3RhYmxlPgo8aW5wdXQgdHlwZT0iaW1hZ2UiIHNyYz0iaHR0cHM6Ly93d3cucGF5cGFsb2JqZWN0cy5jb20vZW5fVVMvaS9idG4vYnRuX2J1eW5vd0NDX0xHLmdpZiIgYm9yZGVyPSIwIiBuYW1lPSJzdWJtaXQiIGFsdD0iUGF5UGFsIC0gVGhlIHNhZmVyLCBlYXNpZXIgd2F5IHRvIHBheSBvbmxpbmUhIj4KPGltZyBhbHQ9IiIgYm9yZGVyPSIwIiBzcmM9Imh0dHBzOi8vd3d3LnBheXBhbG9iamVjdHMuY29tL2VuX1VTL2kvc2NyL3BpeGVsLmdpZiIgd2lkdGg9IjEiIGhlaWdodD0iMSI+CjwvZm9ybT4="
}
META_SEPARATOR
![img](/media/store/dallas-killed-jfk.webp)

# Dallas and the Death of JFK

Oswald may have been the one to fire the rifle, but it was Dallas that killed John F. Kennedy.

Before his arrival, resentment in Dallas towards JFK had been building for years. Men like W.A. Criswell, the pastor of the First Baptist Church of Dallas, preached against a Catholic president. In 1960, three years before Kennedy's assassination, he proclaimed "The election of a Catholic as president would mean the end of religious liberty in America."

![img](/media/store/dallas-killed-jfk-wa-criswell.webp)W.A. Criswell, a Southern Baptist pastor for a church in Dallas, was one of many promiment religious Dallasites who preached against having a Catholic in the White House

A year later, in 1961, the Dallas journalist and publisher Ted Dealey was invited along with other prominent newspaper publishers to meet the president in the White House. He used the opportunity to call Kennedy a "weak sister" for his stance towards communists and compared him to a tricycle rider when what America needed was a "man on horseback to lead this nation."

![img](/media/store/dallas-killed-jfk-ted-dealey.webp)Ted Dealey, a journalist and publisher in Dallas, pointing to Dallas

On the morning of Kennedy's assassination Mr. Dealey ran a full-page ad in his paper *The Dallas Morning News*. Its contents were written by another anti-Kennedy Dallasite, Bernard Weissman, and insinuated Kennedy was a communist.

![img](/media/store/violation.webp)"Welcome to Dallas, bitch" was the message Kennedy received when he opened his local Dallas newspaper the morning of his arrival

In 1963, in the days before Kennedy's arrival, a far-right anti-communist organization called the John Birch Society printed and distributed 5,000 flyers throughout Dallas streets accusing Kennedy of treason.

![img](/media/store/dallas-killed-jfk-wanted-for-treason-flyer.webp)The streets of Dallas were littered with these flyers by the time JFK arrived for his final parade

The day of the assassination, after Kennedy was shot in the head across the Grassy Knoll in Dealey Plaza, he was rushed to Parkland Hospital where Dallas County Coroner Earl Rose did not conduct a full autopsy on the body. Texas law required him to do so, but after a back-and-forth between the Dallas Police Department and the Secret Service, Texas handed the body over to federal authorities. It was performed later, shrouded in secrecy, at the Bethesda Naval Hospital in Maryland. The lack of transparency has contributed to countless conspiracy theories.

![img](/media/store/dallas-killed-jfk-exit-wound-skull.webp)A sketch by coroners at the Naval Hospital in Bethesda

Two days after the assassination, while the world was still reeling from the death of America's youngest president, the Dallas Police Department failed to protect the primary suspect in the case, Lee Harvey Oswald, while transferring him from one jail to another. Doors along the escort route were mysteriously left unlocked and several security guards were removed from the area immediately before the shooting occurred.

![img](/media/store/dallas-killed-jfk-jack-ruby-killing-lee-harvey-oswald.webp)
*Dallas failed to protect the prime suspect in JFK's murder, but succeeded in looking Dallas-as-fuck in matching cream-colored Resistol hat and suit*

Dallas may think it's hot stuff, but it sure couldn't keep a president or its alleged assassin alive.

![img](/media/store/dallas-killed-jfk-socialist-accusal-protesters.webp)Protestors on the day of the assassination called Kennedy a socialist

## T-Shirt Time

Buy a t-shirt to let Dallas know that *you* know who's really responsible for the assassination of JFK.

T-shirts are 100% cotton and were printed in Dallas by DFW Ink. They run small and shrink after washing.

![img](/media/store/dallas-killed-jfk-modeled.webp)

### $14 + 5 shipping + tax
