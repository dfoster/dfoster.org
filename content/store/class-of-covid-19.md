{
	"name": "Class of Covid-19",
	"paypal": "PGZvcm0gYWN0aW9uPSJodHRwczovL3d3dy5wYXlwYWwuY29tL2NnaS1iaW4vd2Vic2NyIiBtZXRob2Q9InBvc3QiIHRhcmdldD0iX3RvcCI+CjxpbnB1dCB0eXBlPSJoaWRkZW4iIG5hbWU9ImNtZCIgdmFsdWU9Il9zLXhjbGljayI+CjxpbnB1dCB0eXBlPSJoaWRkZW4iIG5hbWU9Imhvc3RlZF9idXR0b25faWQiIHZhbHVlPSJSR1hTOVhLWEw2SzhZIj4KPHRhYmxlPgo8dHI+PHRkPjxpbnB1dCB0eXBlPSJoaWRkZW4iIG5hbWU9Im9uMCIgdmFsdWU9IlNpemUiPlNpemU8L3RkPjwvdHI+PHRyPjx0ZD48c2VsZWN0IG5hbWU9Im9zMCI+Cgk8b3B0aW9uIHZhbHVlPSJTbWFsbCI+U21hbGwgPC9vcHRpb24+Cgk8b3B0aW9uIHZhbHVlPSJNZWRpdW0iPk1lZGl1bSA8L29wdGlvbj4KCTxvcHRpb24gdmFsdWU9IkxhcmdlIj5MYXJnZSA8L29wdGlvbj4KCTxvcHRpb24gdmFsdWU9IlhMIj5YTCA8L29wdGlvbj4KCTxvcHRpb24gdmFsdWU9IjJYTCI+MlhMIDwvb3B0aW9uPgoJPG9wdGlvbiB2YWx1ZT0iM1hMIj4zWEwgPC9vcHRpb24+Cjwvc2VsZWN0PiA8L3RkPjwvdHI+CjwvdGFibGU+CjxpbnB1dCB0eXBlPSJpbWFnZSIgc3JjPSJodHRwczovL3d3dy5wYXlwYWxvYmplY3RzLmNvbS9lbl9VUy9pL2J0bi9idG5fYnV5bm93Q0NfTEcuZ2lmIiBib3JkZXI9IjAiIG5hbWU9InN1Ym1pdCIgYWx0PSJQYXlQYWwgLSBUaGUgc2FmZXIsIGVhc2llciB3YXkgdG8gcGF5IG9ubGluZSEiPgo8aW1nIGFsdD0iIiBib3JkZXI9IjAiIHNyYz0iaHR0cHM6Ly93d3cucGF5cGFsb2JqZWN0cy5jb20vZW5fVVMvaS9zY3IvcGl4ZWwuZ2lmIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIj4KPC9mb3JtPg=="
}
META_SEPARATOR
![img](/media/store/class-of-covid-19.webp)

# Class of Covid-19

😭 Bummed you're not walking for graduation 'cause of some virus killing swathes of the worlds population? Us too!

🎓 Numb the pain of missing this once-in-a-lifetime event by wearing a 100% pre-shrunk cotton t-shirt commemorating what you won't be doing this spring.

🕊️ $1 of every t-shirt sold is donated to American Red Cross for supporting victims of COVID-19

🧵 T-shirts are 100% cotton and were printed here in Dallas by DFW Ink.

![img](/media/store/class-of-covid-19-modeled.webp)

### $14 + 5 shipping + tax
