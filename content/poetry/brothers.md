{
  "title": "Brothers",
  "about": "losing a brother",
  "description": "Time heals all wounds but memories remain",
  "tags": ["death", "loss", "mourning"],
  "artworkBy": ""
}
META_SEPARATOR
A swing hangs in the air
Beneath a clear blue sky
As two brothers sit
On blonde grass below.

Time stands still
Within the picture's frame.
A lens to peer
Into celluloid dreams.

We raced along the creek
That ran past our yard
And caught tadpoles to
Watch them grow into frogs.

Time marches forward
Outside the picture’s frame.
A reflex of nature
To end and create life anew.

I found refuge
A sailor under the sea,
And you found purpose
A sentinel in foreign lands.

Time has no focus
Under the bottle’s spell.
Days have no meaning,
And memories are covered in haze.

I let you fall
Into a dark place.
You found peace
The way you knew how.

Time has no end,
No seasons like nature.
An endless reel
Feeding an old projector.