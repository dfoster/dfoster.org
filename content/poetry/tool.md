{
  "title": "tool",
  "about": "purpose",
  "description": "What drives you?",
  "artworkBy": ""
}
META_SEPARATOR
When I'm done with my job
keep me locked in a box

where it's quiet and dark
and I'm robbed of my senses.

I'll sleep without thought,
at peace between worlds,

till you pull me back out
when again I am useful.