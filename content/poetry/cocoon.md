{
  "title": "Cocoon",
  "about": "the evolution of identity",
  "description": "Die in a dream; wake up in a new reality",
  "tags": ["self-discovery", "self-realization", "identity"],
  "artworkBy": ""
}
META_SEPARATOR
Deep in my mother's womb
I wake up in a new shell
In another dream
One detached from the last.

Behind me old husks lay
Acrid and noxious
Their stenches repel me

Towards new paths
That sprawl before me
Like rivers of light
Flowing between clouds.

From one to the next
I wander with fresh skin
And shed along the way

Thoughts that serve no purpose
Till finally I become
Faster than light.