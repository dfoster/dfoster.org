{
  "title": "Spirit Flower",
  "about": "the love a beautiful woman radiates",
  "description": "Glow with love and hope",
  "artworkBy": ""
}
META_SEPARATOR
Soul like a fire that lights the world,
And petals bright like the morning.
Sunshine smiles that warm any room,
And a heart radiant and pure.

Winds push and pull but you stand strong,
Beautiful and unstoppable,
Hardy roots set in solid soil,
Hopeful, with dreams that won't wilt.

Sunflower taller than the rest,
Northern Star of love and talent,
Guide me to the bright of your light,
A beacon to follow on the coldest of nights.