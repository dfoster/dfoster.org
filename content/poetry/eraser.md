{
  "title": "Eraser",
  "about": "letting go of the past",
  "description": "Let go and move on",
  "tags": ["forgiveness"],
  "artworkBy": ""
}
META_SEPARATOR
Eyes closed
Pure white thoughts
Empty as the color snow

I erase the tangles of grief
tying him to me,
me to you.

My hands
scrub them away
till all that's left

Is a new soul
Cleansed and
innocent as youth.