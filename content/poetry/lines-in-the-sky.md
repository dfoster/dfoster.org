{
  "title": "Lines in the Sky",
  "about": "military aviation and keeping cool in a dogfight",
  "description": "Two pilots fight for their lives near the edge of space",
  "artworkBy": ""
}
META_SEPARATOR
Gull wings span clear blue skies,
Leaving trails of mist in the cold frigid air.

Jade goggles paint the world an emerald hue
As alarms ping, flash and cry for attention.

We push our machines past the edge of their limits,
Flirting with death on the edge of the abyss.