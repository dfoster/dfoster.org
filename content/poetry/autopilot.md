{
  "title": "Autopilot",
  "about": "the feeling of courting a girl",
  "description": "Guided towards a girl by the intuition of the heart",
  "artworkBy": ""
}
META_SEPARATOR
I Heed
    The call from the
    Beacon of your heart
    A signal to guide me
    Into your arms

And fly
    Steady and true
    Into orbit 'round you
    Going through motions
    To bring me closer too.

    This is a
Body on autopilot
    Being pulled in
    By the
Gravity of you.