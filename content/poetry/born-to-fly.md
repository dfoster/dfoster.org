{
  "title": "Born to Fly",
  "about": "a passion for flying and aviation",
  "description": "Do you believe in fate?",
  "artworkBy": ""
}
META_SEPARATOR
I feel her in my core,
A soul leaping for heaven.

I'm dreaming of mountains
And my brother too.

An Arrow through the clouds,
Where few others have been.

Flying is falling in reverse,
And I've been falling my whole life.