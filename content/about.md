**Can I get you to feel what I feel?**

That's my motivation when creating art.
                                                            
Henlo, my name is David Alexander. I'm a software developer, a writer and a poet, and a technology enthusiast based in Dallas, Texas.

I'm interested in collaborating with other artists to use technology to explore our humanity.

Besides web technologies (like creating this website) I use
- [Prisma](https://prisma-ai.com/) to finesse sketches and stock photos into the artwork you see on my website
- [GPT-2](https://openai.com/blog/better-language-models/) to aid in my writing and in [one case](/stories/overwatch-ii) write something entirely on its own
- machine learning / AI programs like [fast-neural-style](https://github.com/jcjohnson/fast-neural-style) and [Hyperlapse Pro](https://www.microsoft.com/en-us/research/product/computational-photography-applications/microsoft-hyperlapse-pro/) along with self-writen programs to create videos [like](https://youtu.be/EsNu8umE4PY) [these](https://youtu.be/5ZPVQNWv3ck)

If you'd like to get in touch you can send me an email at [dev.fosterweb@gmail.com](mailto:dev.fosterweb@gmail.com).

Thank you for checking out my site. I hope something here has evoked an emotion within you.