var express = require('express')
var path = require('path')
var cookieParser = require('cookie-parser')
var logger = require('morgan')

global.appRoot = path.resolve(__dirname);

var indexRouter = require('./routes/index')
var poetryRouter = require('./routes/poetry')
var storiesRouter = require('./routes/stories')
var storeRouter = require('./routes/store')
// var photographyRouter = require('./routes/photography')

var app = express()

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())

app.use('/stories/afterlife', express.static(path.join(__dirname, 'afterlife')))
app.use('/', express.static(path.join(__dirname, 'public')))

app.use('/', indexRouter)
app.use('/poetry', poetryRouter)
app.use('/stories', storiesRouter)
app.use('/t-shirts', storeRouter)
// app.use('/photography', photographyRouter)

// catch 404 and forward to error handler
app.use(function(req, res) {
  res.status(404).render('static/404', {
    htmlTitle: `That page don't exist!`,
    description: 'Invalid URL entered'
  })
})

// error handler
app.use(function(err, req, res) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

module.exports = app