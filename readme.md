`pm2 start bin/www --name=dfoster.org --interpreter=/home/dafos/.nvm/versions/node/v8.15.1/bin/node`

Create sitemap
```
https://www.npmjs.com/package/sitemap-generator-cli
```

port used is 3000

## Comments

The website used at some point Isso comments: https://posativ.org/isso/.

The isso install is located at `/opt/isso`, its db is located at `/var/lib/isso/comments.db`, and its process is managed by `pm2`.

## Misc

Lazy loaded images courtsey of this guy:
- https://github.com/tuupola/lazyload
- https://cdn.jsdelivr.net/npm/lazyload@2.0.0-rc.2/lazyload.js
- https://appelsiini.net/projects/lazyload/

Encoding paypal button HTML temporarily using base64:
- https://www.base64decode.org/

HTML Color names:
- https://htmlcolorcodes.com/color-names/

Possible build pipeline items:
- thumbnails
  - rm thumbnails folder
  - magick script to generate thumbnails from images
- run sitemap generator

TODO:
- ensure gzip is enabled on server
- move isso db into project folder
- add blog?
  - https://lumen.netlify.app/
  - https://www.gatsbyjs.org/starters/alxshelepenok/gatsby-starter-lumen/
  - post ideas
    - macOS sucks
    - AI/ML tools for graphic design
    - GPT-3 and the end of creativity

imagemagick commands:
- `magick mogrify -format webp *.jpeg`
- `magick mogrify -path thumbs -thumbnail 200x *.webp`